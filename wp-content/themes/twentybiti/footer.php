<footer class="footer">
		<div class="footer_background"></div>
		<div class="container">
			<div class="row footer_row">
				<div class="col">
					<div class="footer_content">
						<div class="row">

							<div class="col-lg-3 footer_col">
					
								<!-- Footer About -->
								<div class="footer_section footer_about">
									<div class="footer_logo_container">
										<a href="#">
										<img class = "logo" src="<?php bloginfo('template_url'); ?>/assets/images/icon/logo1.png" alt="">
										</a>
									</div>
									<div class="footer_about_text">
										<p>www.glaplatform.com</p>
									</div>
									<div class="footer_social">
										<ul>
											<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
										</ul>
									</div>
								</div>
								
							</div>

							<div class="col-lg-3 footer_col">
								<!-- Footer Contact -->
								<div class="footer_section footer_contact">
									<div class="footer_title">LEARN MORE</div>
									<div class="footer_links_container">
										<ul>
											<li><a href="/courses/">Course</a></li>
											<li><a href="#">Webinar</a></li>
											<li><a href="#">Subscription</a></li>
											<li><a href="#">About</a></li>
										</ul>
									</div>
								</div>	
							</div>

							<div class="col-lg-3 footer_col">
								<!-- Footer Contact -->
								<div class="footer_section footer_contact">
									<div class="footer_title">SPONSOR</div>
									<div class="footer_links_container">
										<ul>
											<li><a href="index.html">Contact us</a></li>
											<li><a href="#">Terms of use</a></li>
											<li><a href="#">Legal information</a></li>
											<li><a href="#">Privacy Policy</a></li>
										</ul>
									</div>
								</div>	
							</div>
							<div class="col-lg-3 footer_col">
								<!-- Footer Contact -->
								<div class="footer_section footer_contact">
									<div class="footer_title">EXPLORE</div>
									<div class="footer_links_container">
										<ul>
											<li><a href="index.html">Scientific Board</a></li>
											<li><a href="#">Scientific partners</a></li>
											<li><a href="#">Worldwide events</a></li>
											<li><a href="#">Humanitarian missionsn</a></li>
										</ul>
									</div>
								</div>	
							</div>
							
						</div>
					</div>
				</div>
			</div>

			<div class="row copyright_row">
				<div class="col">
					<div class="copyright d-flex flex-lg-row flex-column align-items-center justify-content-start">
						<div class="cr_text"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved </a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></div>
						<div class="ml-lg-auto cr_links">
							<ul class="cr_list">
								<li><a href="#">Copyright notification</a></li>
								<li><a href="#">Terms of Use</a></li>
								<li><a href="#">Privacy Policy</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
</div>

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/granim/2.0.0/granim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
<!--
<script src="<?php bloginfo('template_url'); ?>/bitihome/js/index.js"></script>
-->
<!--
<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
-->
<!--
<script src="plugins/greensock/TweenMax.min.js"></script>
<script src="plugins/greensock/TimelineMax.min.js"></script>
-->
<script src="<?php bloginfo('template_url'); ?>/bitihome/plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/bitihome/plugins/greensock/animation.gsap.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/bitihome/plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/bitihome/js/custom.js"></script>
<!--
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
-->
<!-- <script src="<?php bloginfo('template_url'); ?>/bitihome/plugins/easing/easing.js"></script> -->
<!--
<script src="plugins/parallax-js-master/parallax.min.js"></script>
-->
<?php wp_footer(); ?>

</body>
</html>