<!DOCTYPE html>
<html lang="en">
<head>
<title>BITI | TRANG CHỦ </title>

<meta charset="utf-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge">

<meta name="description" content="BITI project">

<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css" />

<link href="<?php bloginfo('template_url'); ?>/bitihome/plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!--
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">

<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.theme.default.css">

<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">

-->

<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/bitihome/styles/main_styles.css?v=2">

<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/bitihome/styles/responsive.css">

<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/bitihome/styles/banner.css">

<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/bitihome/styles/pricing.css">

<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/bitihome/styles/card-gallery.css">

<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/bitihome/styles/footer.css">

<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/assets/css/portfilio/main.css?v=9" rel="stylesheet">
<?php wp_head(); ?>
</head>
<body>

<div class="super_container">

	<!-- Header -->

	<header class="header">
			
		<!-- Top Bar -->
		<div class="top_bar">
			<div class="top_bar_container">
				<div class="container">
					<div class="row">
						<div class="col">
							<div class="top_bar_content d-flex flex-row align-items-center justify-content-start">
								<ul class="top_bar_contact_list">
									<li><div class="question">Liên hệ ngay: </div></li>
									<li>
										<i class="fa fa-phone" aria-hidden="true"></i>
										<div>(+84) 915 531 379</div>
									</li>
									<li>
										<i class="fa fa-envelope-o" aria-hidden="true"></i>
										<div>baoquoc@biti.vn</div>
									</li>
								</ul>
								<div class="top_bar_login ml-auto">
									<div class="login_button"><a href="#">Đăng ký/Đăng nhập</a></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>				
		</div>

		<!-- Header Content -->
		<div class="header_container">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="header_content d-flex flex-row align-items-center justify-content-start">
							<div class="logo_container">
								<a href="#">
									<div class="logo_text">BI<span>TI</span></div>
								</a>
							</div>
							<nav class="main_nav_contaner ml-auto">
							<ul class="main_nav">
							<?php 
								$args = array(
									'theme_location'=>'primary',
								);
							?>
							<?php wp_nav_menu($args); ?>   
							</ul>
							<div class="search_button"><i class="fa fa-search" aria-hidden="true"></i></div>
							</nav>

						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Header Search Panel -->
		<div class="header_search_container">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="header_search_content d-flex flex-row align-items-center justify-content-end">
							<form action="#" class="header_search_form">
								<input type="search" class="search_input" placeholder="Search" required="required">
								<button class="header_search_button d-flex flex-column align-items-center justify-content-center">
									<i class="fa fa-search" aria-hidden="true"></i>
								</button>
							</form>
						</div>
					</div>
				</div>
			</div>			
		</div>			
	</header>

	<!-- Menu -->

	<div class="menu d-flex flex-column align-items-end justify-content-start text-right menu_mm trans_400" >
		<div class="menu_close_container"><div class="menu_close"><div></div><div></div></div></div>
		<div class="search">
			<form action="#" class="header_search_form menu_mm">
				<input type="search" class="search_input menu_mm" placeholder="Search" required="required">
				<button class="header_search_button d-flex flex-column align-items-center justify-content-center menu_mm">
					<i class="fa fa-search menu_mm" aria-hidden="true"></i>
				</button>
			</form>
		</div>
		<nav class="menu_nav" style="display:none">
			<ul class="menu_mm">
				<li class="menu_mm"><a href="index.html">Home</a></li>
				<li class="menu_mm"><a href="#">About</a></li>
				<li class="menu_mm"><a href="#">Courses</a></li>
				<li class="menu_mm"><a href="#">Blog</a></li>
				<li class="menu_mm"><a href="#">Page</a></li>
				<li class="menu_mm"><a href="contact.html">Contact</a></li>
			</ul>
		</nav>
	</div>