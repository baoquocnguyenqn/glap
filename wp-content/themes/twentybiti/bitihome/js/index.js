var bannerGranim = new Granim({
	element: '#banner-canvas',
	name: 'basic-gradient',
	direction: 'left-right',
	opacity: [1, 1],
	states : {
		"default-state": {
			gradients: [
				['#193A6F', '#5B84C4'],
				['#2C599D', '#11224D'],
				['#F98125', '#FB9850']
			],
			transitionSpeed: 7000,
			loop: true
		},
	}
});