<?php 
	//add css style
	function special_news_scripts() {
		wp_enqueue_style('style',get_stylesheet_uri());
	}

	add_action('wp_enqueue_scripts','special_news_scripts');



	add_action('wp_enqueue_scripts','special_news_scripts');


	//Navigation
	register_nav_menus(array(
		'primary'=> __('Primary Menu'),
		'footer'=> __('Footer Menu'),
	));
    

/*
*Add support woocommerce
*/
function bititheme_add_woocommerce_support() {
  add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'bititheme_add_woocommerce_support' );

/**
 * Remove product data tabs
 */
add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

function woo_remove_product_tabs( $tabs ) {

    unset( $tabs['description'] );      	// Remove the description tab
    unset( $tabs['additional_information'] );  	// Remove the additional information tab

    return $tabs;
}

add_theme_support( 'post-thumbnails' );
