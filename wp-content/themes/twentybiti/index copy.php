<?php
/**
 * Template Name: Tempalte full page biti
 * Template Post Type: post, page
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */
get_header();
?>

<?php
	if ( have_posts() ) :
	while ( have_posts() ) : the_post();?>
	<article class="post page">
		<?php the_content(); ?>
	</article>
	<?php
	endwhile;
	else :
	endif; 
?>

<!-- Footer -->

<?php get_footer(); ?>