<?php
/**
 * Template Name: Tempalte page home
 * Template Post Type: post, page
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */
get_header();
?>



	
	<!-- Home -->

	<section id="banner-wrap">
		<canvas id="banner-canvas"></canvas>
			<div id="banner-carousel" class="carousel slide" data-ride="carousel">
				<ol class="carousel-indicators">
					<li data-target="#banner-carousel" data-slide-to="0" class="active"></li>
					<li data-target="#banner-carousel" data-slide-to="1"></li>
					<li data-target="#banner-carousel" data-slide-to="2"></li>
				</ol>
				<div class="carousel-inner">
					<div class="carousel-item active">
						<div class="container">
							<div class="col-md-12 text-center">
								<h1>BITI</h1>
								<p>Chuyên thiết kế website và ứng dụng chuẩn SEO.</p>
								<button class="home_search_button" type="submit">Liên hệ</button>
							</div>
						</div>
					</div>
					<div class="carousel-item">
						<div class="container">
							<div class="col-md-12 text-center">
								<h1>Amazing</h1>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
							</div>
						</div>
					</div>
					<div class="carousel-item">
						<div class="container">
							<div class="col-md-12 text-center">
								<h1>Awesome</h1>
								<p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
							</div>
						</div>
					</div>
				</div>
				<a class="carousel-control-prev" href="#banner-carousel" role="button" data-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="carousel-control-next" href="#banner-carousel" role="button" data-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>
	</section>

	<!-- Features -->

	<div class="features">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="section_title_container text-center">
						<h2 class="section_title">Chúng tôi là <span>BITI</span></h2>
						<div class="section_subtitle"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel gravida arcu. Vestibulum feugiat, sapien ultrices fermentum congue, quam velit venenatis sem</p></div>
					</div>
				</div>
			</div>

			<div class="row features_row">
				
				<!-- Features Item -->
				<div class="col-lg-4 feature_col">
					<div class="feature text-center trans_400">
						<div class="feature_icon"><img src="<?php bloginfo('template_url'); ?>/bitihome/images/icon_2.png" alt=""></div>
						<h3 class="feature_title">Dịch vụ đa dạng</h3>
						<div class="feature_text"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p></div>
					</div>
				</div>
			
				<!-- Features Item -->
				<div class="col-lg-4 feature_col">
					<div class="feature text-center trans_400">
						<div class="feature_icon"><img src="<?php bloginfo('template_url'); ?>/bitihome/images/icon_3.png" alt=""></div>
						<h3 class="feature_title">Chi phí hợp lý</h3>
						<div class="feature_text"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p></div>
					</div>
				</div>
			
				<!-- Features Item -->
				<div class="col-lg-4 feature_col">
					<div class="feature text-center trans_400">
						<div class="feature_icon"><img src="<?php bloginfo('template_url'); ?>/bitihome/images/icon_4.png" alt=""></div>
						<h3 class="feature_title">Hỗ trợ Marketing</h3>
						<div class="feature_text"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p></div>
					</div>
				</div>
			
			</div>

			<div class="row features_row">
				<!-- About Item -->
				<div class="col-lg-4 about_col about_col_left">
					<div class="about_item">
						<div class="about_item_image"><img src="<?php bloginfo('template_url'); ?>/bitihome/images/about_1.jpg" alt=""></div>
						<div class="about_item_title"><a href="#">Triết lý</a></div>
						<div class="about_item_text">
							<p>Lorem ipsum dolor sit , consectet adipisi elit, sed do eiusmod tempor for enim en consectet adipisi elit, sed do consectet adipisi elit, sed doadesg.</p>
						</div>
					</div>
				</div>

				<!-- About Item -->
				<div class="col-lg-4 about_col about_col_middle">
					<div class="about_item">
						<div class="about_item_image"><img src="<?php bloginfo('template_url'); ?>/bitihome/images/about_2.jpg" alt=""></div>
						<div class="about_item_title"><a href="#">Chiến lược</a></div>
						<div class="about_item_text">
							<p>Lorem ipsum dolor sit , consectet adipisi elit, sed do eiusmod tempor for enim en consectet adipisi elit, sed do consectet adipisi elit, sed doadesg.</p>
						</div>
					</div>
				</div>

				<!-- About Item -->
				<div class="col-lg-4 about_col about_col_right">
					<div class="about_item">
						<div class="about_item_image"><img src="<?php bloginfo('template_url'); ?>/bitihome/images/about_3.jpg" alt=""></div>
						<div class="about_item_title"><a href="#">Tầm nhìn và sứ mệnh</a></div>
						<div class="about_item_text">
							<p>Lorem ipsum dolor sit , consectet adipisi elit, sed do eiusmod tempor for enim en consectet adipisi elit, sed do consectet adipisi elit, sed doadesg.</p>
						</div>
					</div>
				</div>		
			</div>	
		</div>
	</div>

	<!-- Team -->

	<div class="team">
		<div class="team_background parallax-window" data-parallax="scroll" data-image-src="images/team_background.jpg" data-speed="0.8"></div>
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="section_title_container text-center">
						<h2 class="section_title">Hãy lựa chọn BITI</h2>
						<div class="section_subtitle"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel gravida arcu. Vestibulum feugiat, sapien ultrices fermentum congue, quam velit venenatis sem</p></div>
					</div>
				</div>
			</div>
			<div class="row team_row">
				
				<!-- Team Item -->
				<div class="col-lg-4 col-md-6 team_col">
					<div class="team_item">
						<div class="team_image"><img src="<?php bloginfo('template_url'); ?>/bitihome/images/f1.png" alt=""></div>
						<div class="team_body">
							<div class="team_title"><a href="#">Giao diện đẹp</a></div>
							<div class="team_subtitle">Lorem ipsum dolor sit amet</div>
						</div>
					</div>
				</div>

				<!-- Team Item -->
				<div class="col-lg-4 col-md-6 team_col">
					<div class="team_item">
						<div class="team_image"><img src="<?php bloginfo('template_url'); ?>/bitihome/images/f2.png" alt=""></div>
						<div class="team_body">
							<div class="team_title"><a href="#">Chuẩn SEO</a></div>
							<div class="team_subtitle">Lorem ipsum dolor sit amet</div>
						</div>
					</div>
				</div>

				<!-- Team Item -->
				<div class="col-lg-4 col-md-6 team_col">
					<div class="team_item">
						<div class="team_image"><img src="<?php bloginfo('template_url'); ?>/bitihome/images/f3.png" alt=""></div>
						<div class="team_body">
							<div class="team_title"><a href="#">Bảo mật</a></div>
							<div class="team_subtitle">Lorem ipsum dolor sit amet</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row team_row">
				<!-- Team Item -->
				<div class="col-lg-4 col-md-6 team_col">
					<div class="team_item">
						<div class="team_image"><img src="<?php bloginfo('template_url'); ?>/bitihome/images/f4.png" alt=""></div>
						<div class="team_body">
							<div class="team_title"><a href="#">Công nghệ mới</a></div>
							<div class="team_subtitle">Lorem ipsum dolor sit amet</div>
						</div>
					</div>
				</div>

				<!-- Team Item -->
				<div class="col-lg-4 col-md-6 team_col">
					<div class="team_item">
						<div class="team_image"><img src="<?php bloginfo('template_url'); ?>/bitihome/images/f1.png" alt=""></div>
						<div class="team_body">
							<div class="team_title"><a href="#">Đa thiết bị</a></div>
							<div class="team_subtitle">Lorem ipsum dolor sit amet</div>
						</div>
					</div>
				</div>

				<!-- Team Item -->
				<div class="col-lg-4 col-md-6 team_col">
					<div class="team_item">
						<div class="team_image"><img src="<?php bloginfo('template_url'); ?>/bitihome/images/f1.png" alt=""></div>
						<div class="team_body">
							<div class="team_title"><a href="#">Bảo hành trọn đời</a></div>
							<div class="team_subtitle">Lorem ipsum dolor sit amet</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	

	<!-- Popular Courses -->

	<div class="courses">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="section_title_container text-center">
						<h2 class="section_title">Các gói dịch vụ</h2>
						<div class="section_subtitle"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel gravida arcu. Vestibulum feugiat, sapien ultrices fermentum congue, quam velit venenatis sem</p></div>
					</div>
				</div>
			</div>
			<div class="row courses_row">
				<div class="col-sm-4">
					<div class="card text-center">
						<div class="title">
							<i class="fa fa-paper-plane" aria-hidden="true"></i>
							<h2>Cơ bản</h2>
						</div>
						<div class="price">
							<h4><sup>$</sup>25</h4>
						</div>
						<div class="option">
							<ul>
								<li><i class="fa fa-check" aria-hidden="true"></i>10 GB Space</li>
								<li><i class="fa fa-check" aria-hidden="true"></i>3 Domain Names</li>
								<li><i class="fa fa-check" aria-hidden="true"></i>20 Email Address</li>
								<li><i class="fa fa-times" aria-hidden="true"></i>Live Support</li>
							</ul>
						</div>
						<a href="#">Xem thêm</a>
					</div>
				</div>

				<div class="col-sm-4">
					<div class="card text-center">
						<div class="title">
							<i class="fa fa-plane" aria-hidden="true"></i>
							<h2>Standard</h2>
						</div>
						<div class="price">
							<h4><sup>$</sup>25</h4>
						</div>
						<div class="option">
							<ul>
								<li><i class="fa fa-check" aria-hidden="true"></i>10 GB Space</li>
								<li><i class="fa fa-check" aria-hidden="true"></i>5 Domain Names</li>
								<li><i class="fa fa-check" aria-hidden="true"></i>Unlimited Email Address</li>
								<li><i class="fa fa-times" aria-hidden="true"></i>Live Support</li>
							</ul>
						</div>
						<a href="#">Xem thêm</a>
					</div>
				</div>

				<div class="col-sm-4">
					<div class="card text-center">
						<div class="title">
							<i class="fa fa-rocket" aria-hidden="true"></i>
							<h2>Premium</h2>
						</div>
						<div class="price">
							<h4><sup>$</sup>100</h4>
						</div>
						<div class="option">
							<ul>
								<li><i class="fa fa-check" aria-hidden="true"></i>Unlimited GB Space</li>
								<li><i class="fa fa-check" aria-hidden="true"></i>30 Domain Names</li>
								<li><i class="fa fa-check" aria-hidden="true"></i>Unlimited Email Address</li>
								<li><i class="fa fa-check" aria-hidden="true"></i>Live Support</li>
							</ul>
						</div>
						<a href="#">Xem thêm</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<section class="gallery-block cards-gallery">
		<div class="section_background parallax-window" data-parallax="scroll" data-image-src="<?php bloginfo('template_url'); ?>/bitihome/images/courses_background.jpg" data-speed="0.8"></div>	
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="section_title_container text-center">
						<h2 class="section_title">Kho giao diện</h2>
						<div class="section_subtitle"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel gravida arcu. Vestibulum feugiat, sapien ultrices fermentum congue, quam velit venenatis sem</p></div>
					</div>
				</div>
			</div>
			<div class="row courses_row">
				<div class="col-md-6 col-lg-3">
					<div class="card_2 border-0 transform-on-hover">
						<a class="lightbox" href="<?php bloginfo('template_url'); ?>/bitihome/images/event_1.jpg">
							<img src="<?php bloginfo('template_url'); ?>/bitihome/images/event_1.jpg" alt="Card Image" class="card-img-top">
						</a>
						<div class="card-body">
							<h6>
								<a href="#">Lorem Ipsum</a>
							</h6>
							<p class="text-muted card-text">Lorem ipsum dolor sit amet.</p>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-lg-3">
					<div class="card_2 border-0 transform-on-hover">
						<a class="lightbox" href="<?php bloginfo('template_url'); ?>/bitihome/images/event_2.jpg">
							<img src="<?php bloginfo('template_url'); ?>/bitihome/images/event_2.jpg" alt="Card Image" class="card-img-top">
						</a>
						<div class="card-body">
							<h6>
								<a href="#">Lorem Ipsum</a>
							</h6>
							<p class="text-muted card-text">Lorem ipsum dolor sit amet.</p>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-lg-3">
					<div class="card_2 border-0 transform-on-hover">
						<a class="lightbox" href="<?php bloginfo('template_url'); ?>/bitihome/images/event_3.jpg">
							<img src="<?php bloginfo('template_url'); ?>/bitihome/images/event_3.jpg" alt="Card Image" class="card-img-top">
						</a>
						<div class="card-body">
							<h6>
								<a href="#">Lorem Ipsum</a>
							</h6>
							<p class="text-muted card-text">Lorem ipsum dolor sit amet.</p>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-lg-3">
					<div class="card_2 border-0 transform-on-hover">
						<a class="lightbox" href="<?php bloginfo('template_url'); ?>/bitihome/images/event_4.jpg">
							<img src="<?php bloginfo('template_url'); ?>/bitihome/images/event_4.jpg" alt="Card Image" class="card-img-top">
						</a>
						<div class="card-body">
							<h6>
								<a href="#">Lorem Ipsum</a>
							</h6>
							<p class="text-muted card-text">Lorem ipsum dolor sit amet.</p>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-lg-3">
					<div class="card_2 border-0 transform-on-hover">
						<a class="lightbox" href="<?php bloginfo('template_url'); ?>/bitihome/images/event_3.jpg">
							<img src="<?php bloginfo('template_url'); ?>/bitihome/images/event_3.jpg" alt="Card Image" class="card-img-top">
						</a>
						<div class="card-body">
							<h6>
								<a href="#">Lorem Ipsum</a>
							</h6>
							<p class="text-muted card-text">Lorem ipsum dolor sit amet.</p>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-lg-3">
					<div class="card_2 border-0 transform-on-hover">
						<a class="lightbox" href="<?php bloginfo('template_url'); ?>/bitihome/images/event_4.jpg">
							<img src="<?php bloginfo('template_url'); ?>/bitihome/images/event_4.jpg" alt="Card Image" class="card-img-top">
						</a>
						<div class="card-body">
							<h6>
								<a href="#">Lorem Ipsum</a>
							</h6>
							<p class="text-muted card-text">Lorem ipsum dolor sit amet.</p>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-lg-3">
					<div class="card_2 border-0 transform-on-hover">
						<a class="lightbox" href="<?php bloginfo('template_url'); ?>/bitihome/images/event_1.jpg">
							<img src="<?php bloginfo('template_url'); ?>/bitihome/images/event_1.jpg" alt="Card Image" class="card-img-top">
						</a>
						<div class="card-body">
							<h6>
								<a href="#">Lorem Ipsum</a>
							</h6>
							<p class="text-muted card-text">Lorem ipsum dolor sit amet.</p>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-lg-3">
					<div class="card_2 border-0 transform-on-hover">
						<a class="lightbox" href="<?php bloginfo('template_url'); ?>/bitihome/images/event_2.jpg">
							<img src="<?php bloginfo('template_url'); ?>/bitihome/images/event_2.jpg" alt="Card Image" class="card-img-top">
						</a>
						<div class="card-body">
							<h6>
								<a href="#">Lorem Ipsum</a>
							</h6>
							<p class="text-muted card-text">Lorem ipsum dolor sit amet.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- News Letter-->

	<div class="newsletter">
		<div class="newsletter_background" style="background-image:url(<?php bloginfo('template_url'); ?>/bitihome/images/newsletter_background.jpg)"></div>
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="newsletter_container d-flex flex-lg-row flex-column align-items-center justify-content-start">

						<!-- Newsletter Content -->
						<div class="newsletter_content text-lg-left text-center">
							<div class="newsletter_title">Đăng ký nhận thông tin</div>
							<div class="newsletter_subtitle">Subcribe to lastest smartphones news &amp; great deals we offer</div>
						</div>

						<!-- Newsletter Form -->
						<div class="newsletter_form_container ml-lg-auto">
							<form action="#" id="newsletter_form" class="newsletter_form d-flex flex-row align-items-center justify-content-center">
								<input type="email" class="newsletter_input" placeholder="Your Email" required="required">
								<button type="submit" class="newsletter_button">Đăng ký</button>
							</form>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Footer -->

    <?php get_footer(); ?>