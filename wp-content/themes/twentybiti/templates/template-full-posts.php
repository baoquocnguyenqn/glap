<?php
/**
 * Template Name: Tempalte posts
 * Template Post Type: post, page
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */
get_header();
?>

	<!-- Home -->

	<section id="banner-wrap">
		<canvas id="banner-canvas"></canvas>
			<div id="banner-carousel" class="carousel slide" data-ride="carousel">
				<ol class="carousel-indicators">
					<li data-target="#banner-carousel" data-slide-to="0" class="active"></li>
					<li data-target="#banner-carousel" data-slide-to="1"></li>
					<li data-target="#banner-carousel" data-slide-to="2"></li>
				</ol>
				<div class="carousel-inner">
					<div class="carousel-item active">
						<div class="container">
							<div class="col-md-12 text-center">
								<h1>BITI</h1>
								<p>Chuyên thiết kế website và ứng dụng chuẩn SEO.</p>
								<button class="home_search_button" type="submit">Liên hệ</button>
							</div>
						</div>
					</div>
					<div class="carousel-item">
						<div class="container">
							<div class="col-md-12 text-center">
								<h1>Amazing</h1>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
							</div>
						</div>
					</div>
					<div class="carousel-item">
						<div class="container">
							<div class="col-md-12 text-center">
								<h1>Awesome</h1>
								<p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
							</div>
						</div>
					</div>
				</div>
				<a class="carousel-control-prev" href="#banner-carousel" role="button" data-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="carousel-control-next" href="#banner-carousel" role="button" data-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>
	</section>
	<!-- Team -->
	<div class="team">
		<div class="team_background parallax-window" data-parallax="scroll" data-image-src="images/team_background.jpg" data-speed="0.8"></div>
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="section_title_container text-center">
						<h2 class="section_title"><?php the_title(); ?></h2>
						<div class="section_subtitle"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel gravida arcu. Vestibulum feugiat, sapien ultrices fermentum congue, quam velit venenatis sem</p></div>
					</div>
				</div>
			</div>
		
			<div class="row">
			<?php 
				// wp-query to get all published posts without pagination
				$allPostsWPQuery = new WP_Query(array('post_type'=>'post', 'post_status'=>'publish', 'posts_per_page'=>-1)); ?>
				
				<?php if ( $allPostsWPQuery->have_posts() ) : ?>
					<?php while ( $allPostsWPQuery->have_posts() ) : $allPostsWPQuery->the_post(); ?>
				<div class="col-lg-4 about_col about_col_left">
					<div class="about_item">
						<div class="about_item_image"><img src="http://biti.test/wp-content/themes/twentybiti/bitihome/images/about_1.jpg" alt=""></div>
						<div class="about_item_title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
						<div class="about_item_text">
							<?php  substr( the_content(), 0,30); ?>
						</div>
					</div>
				</div>
				<?php endwhile; ?>

					<?php wp_reset_postdata(); ?>
				<?php else : ?>
					<p><?php _e( 'There no posts to display.' ); ?></p>
				<?php endif; ?>
			</div>
		</div>
	</div>	
	<!-- Footer -->

    <?php get_footer(); ?>