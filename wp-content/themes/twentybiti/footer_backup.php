<footer class="footer">
		<div class="footer_background" style="background-image:url(<?php bloginfo('template_url'); ?>/bitihome/images/footer_background.png)"></div>
		<div class="container">
			<div class="row footer_row">
				<div class="col">
					<div class="footer_content">
						<div class="row">

							<div class="col-lg-3 footer_col">
					
								<!-- Footer About -->
								<div class="footer_section footer_about">
									<div class="footer_logo_container">
										<a href="#">
											<div class="footer_logo_text">BI<span>TI</span></div>
										</a>
									</div>
									<div class="footer_about_text">
										<p>Công ty Thương Mại & Công Nghệ BITI, chuyên thiết kế website, marketing online, thiết kế các ứng dụng chuyên nghiệp, app mobile.</p>
									</div>
									<div class="footer_social">
										<ul>
											<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
											<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
										</ul>
									</div>
								</div>
								
							</div>

							<div class="col-lg-3 footer_col ">
					
								<!-- Footer Contact -->
								<div class="footer_section footer_contact">
									<div class="footer_title">VPĐD tại Việt Nam</div>
									<div class="footer_contact_info">
										<ul>
											<li><i class="fa fa-home"></i> Địa chỉ:26 Tùng Lâm 4, Hòa Xuân, Cẩm Lệ, Đà Nẵng, Việt Nam </li>											
											<li><i class="fa fa-phone"></i> Hotline: (+84) 915 531 379</li>											
											<li><i class="fa fa-envelope"></i> Email: nguyenbaonguyen.vn@gmail.com</li>
										</ul>
									</div>
								</div>
								 
							</div>

							<div class="col-lg-3 footer_col">
					
								<!-- Footer Contact -->
								<div class="footer_section footer_contact">
									<div class="footer_title">VPĐD tại Pháp</div>
									<div class="footer_contact_info">
										<ul>
											<li><i class="fa fa-home"></i>Địa chỉ: 9 Allee jean baptiste clement, Andresy, France </li>											
											<li><i class="fa fa-phone"></i> Hotline:  (+) 659 34 7990</li>											
											<li><i class="fa fa-envelope"></i> Email: baoquoc@biti.vn</li>


										</ul>
									</div>
								</div>
								
							</div>

							<div class="col-lg-3 footer_col clearfix">
					
								<!-- Footer links -->
								<div class="footer_section footer_links">
									<div class="footer_title">Contact Us</div>
									<div class="footer_links_container">
									<?php 
										$args = array(
											'theme_location'=>'footer',
										);
									?>
									<?php wp_nav_menu($args); ?>   
										<ul style="display:none">
											<li><a href="index.html">Trang chủ</a></li>
											<li><a href="#">Giới thiệu</a></li>
											<li><a href="#">Dịch vụ</a></li>
											<li><a href="#">Kho giao diện</a></li>
											<li><a href="#">Dự án</a></li>
											<li><a href="#">Đối tác</a></li>
											<li><a href="#">Liên hệ</a></li>
										</ul>
									</div>
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row copyright_row">
				<div class="col">
					<div class="copyright d-flex flex-lg-row flex-column align-items-center justify-content-start">
						<div class="cr_text"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved </a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></div>
						<div class="ml-lg-auto cr_links">
							<ul class="cr_list">
								<li><a href="#">Copyright notification</a></li>
								<li><a href="#">Terms of Use</a></li>
								<li><a href="#">Privacy Policy</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
</div>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/granim/2.0.0/granim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/bitihome/js/index.js"></script>
<!--
<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
-->
<!--
<script src="plugins/greensock/TweenMax.min.js"></script>
<script src="plugins/greensock/TimelineMax.min.js"></script>
-->
<script src="<?php bloginfo('template_url'); ?>/bitihome/plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/bitihome/plugins/greensock/animation.gsap.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/bitihome/plugins/greensock/ScrollToPlugin.min.js"></script>
<!--
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
-->
<!-- <script src="<?php bloginfo('template_url'); ?>/bitihome/plugins/easing/easing.js"></script> -->
<!--
<script src="plugins/parallax-js-master/parallax.min.js"></script>
-->
<?php wp_footer(); ?>
<script src="<?php bloginfo('template_url'); ?>/bitihome/js/custom.js"></script>
</body>
</html>