<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since Twenty Seventeen 1.0
 * @version 1.0
 */
get_header();
?>

	<!-- Home -->

	<!--<section id="banner-wrap">-->
	<!--	<canvas id="banner-canvas"></canvas>-->
	<!--		<div id="banner-carousel" class="carousel slide" data-ride="carousel">-->
	<!--			<ol class="carousel-indicators">-->
	<!--				<li data-target="#banner-carousel" data-slide-to="0" class="active"></li>-->
	<!--				<li data-target="#banner-carousel" data-slide-to="1"></li>-->
	<!--				<li data-target="#banner-carousel" data-slide-to="2"></li>-->
	<!--			</ol>-->
	<!--			<div class="carousel-inner">-->
	<!--				<div class="carousel-item active">-->
	<!--					<div class="container">-->
	<!--						<div class="col-md-12 text-center">-->
	<!--							<h1>BITI</h1>-->
	<!--							<p>Chuyên thiết kế website và ứng dụng chuẩn SEO.</p>-->
	<!--							<button class="home_search_button" type="submit">Liên hệ</button>-->
	<!--						</div>-->
	<!--					</div>-->
	<!--				</div>-->
	<!--				<div class="carousel-item">-->
	<!--					<div class="container">-->
	<!--						<div class="col-md-12 text-center">-->
	<!--							<h1>Amazing</h1>-->
	<!--							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>-->
	<!--						</div>-->
	<!--					</div>-->
	<!--				</div>-->
	<!--				<div class="carousel-item">-->
	<!--					<div class="container">-->
	<!--						<div class="col-md-12 text-center">-->
	<!--							<h1>Awesome</h1>-->
	<!--							<p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>-->
	<!--						</div>-->
	<!--					</div>-->
	<!--				</div>-->
	<!--			</div>-->
	<!--			<a class="carousel-control-prev" href="#banner-carousel" role="button" data-slide="prev">-->
	<!--				<span class="carousel-control-prev-icon" aria-hidden="true"></span>-->
	<!--				<span class="sr-only">Previous</span>-->
	<!--			</a>-->
	<!--			<a class="carousel-control-next" href="#banner-carousel" role="button" data-slide="next">-->
	<!--				<span class="carousel-control-next-icon" aria-hidden="true"></span>-->
	<!--				<span class="sr-only">Next</span>-->
	<!--			</a>-->
	<!--		</div>-->
	<!--</section>-->
	<!-- Team -->
	<div class="team">
		<div class="team_background parallax-window" data-parallax="scroll" data-image-src="images/team_background.jpg" data-speed="0.8"></div>
		<div class="container">
			<!--<div class="row">-->
			<!--	<div class="col">-->
			<!--		<div class="section_title_container text-center">-->
			<!--			<h2 class="section_title">Các dự án đã thực hiện</h2>-->
			<!--			<div class="section_subtitle"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel gravida arcu. Vestibulum feugiat, sapien ultrices fermentum congue, quam velit venenatis sem</p></div>-->
			<!--		</div>-->
			<!--	</div>-->
			<!--</div>-->
			<div class="">
                <?php
                if ( have_posts() ) :
                while ( have_posts() ) : the_post();?>
                <article class="post page">
                    <?php the_content(); ?>
                </article>
                <?php
                endwhile;
                else :
                endif; 
                ?>
			</div>
		</div>
	</div>	
	<!-- Footer -->

    <?php get_footer(); ?>