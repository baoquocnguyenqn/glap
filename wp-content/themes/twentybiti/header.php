<!DOCTYPE html>
<html lang="en">
<head>
<title><?php get_bloginfo('name'); ?></title>


<meta charset="utf-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge">

<meta name="description" content="Glaplatform project">

<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css" />

<link href="<?php bloginfo('template_url'); ?>/bitihome/plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!--
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">

<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.theme.default.css">

<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">

-->

<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/bitihome/styles/main_styles.css?v=4">

<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/bitihome/styles/responsive.css">

<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/bitihome/styles/banner.css">

<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/bitihome/styles/pricing.css">

<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/bitihome/styles/card-gallery.css">

<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/bitihome/styles/footer.css?v=12">

<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/assets/css/portfilio/main.css?v=9" rel="stylesheet">
<?php wp_head(); ?>
</head>
<style>

</style>
<body <?php body_class(); ?>>

<div class="super_container">

	<!-- Header -->

	<header class="header">
		<!-- Header Content -->
		<div class="header_container">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="header_content d-flex flex-row align-items-center justify-content-start">
							<div class="logo_container">
								<a href="#">
									<img class = "logo" src="<?php bloginfo('template_url'); ?>/assets/images/icon/logo1.png" alt="">
								</a>
							</div>
							<nav class="main_nav_contaner">
								<ul class="main_nav">
									<?php 
										$args = array(
											'theme_location'=>'primary',
										);
									?>
									<?php wp_nav_menu($args); ?>   
									
								</ul>
								<div class="hamburger menu_mm">
									<i class="fa fa-bars menu_mm" aria-hidden="true"></i>
								</div>		
							</nav>
							<div class="header_right">
								<ul>
									<li class="hotline">
										<i class="fa fa-phone" aria-hidden="true"></i>
										<a href="tel:0915531379">(+84) 915 531 379</a>
									</li>
									<li class="member">
										<a href="http://glaplatform.com/login/">Login</a>
									</li>
									<li class="member">
										<a href="http://glaplatform.com/register/">Sign Up</a>
									</li>
								</ul>
								
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Header Search Panel -->
		<div class="header_search_container">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="header_search_content d-flex flex-row align-items-center justify-content-end">
							<form action="#" class="header_search_form">
								<input type="search" class="search_input" placeholder="Search" required="required">
								<button class="header_search_button d-flex flex-column align-items-center justify-content-center">
									<i class="fa fa-search" aria-hidden="true"></i>
								</button>
							</form>
						</div>
					</div>
				</div>
			</div>			
		</div>			
	</header>

	<!-- Menu -->

	
	<div class="menu d-flex flex-column align-items-end justify-content-start text-right menu_mm trans_400" >
		<div class="menu_close_container"><div class="menu_close"><div></div><div></div></div></div>
		<nav class="menu_nav">
		<?php 
			$args = array(
				'theme_location'=>'primary',
			);
		?>
		<?php wp_nav_menu($args); ?>   
		</nav>
	</div>