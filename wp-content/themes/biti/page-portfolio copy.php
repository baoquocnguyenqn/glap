<?php
    get_header();
    if ( have_posts() ) :
    while ( have_posts() ) : the_post();?>
    <main>
        <h2 class="page-heading"><?php the_title(); ?></h2>
        <div id="post-container">
            <section id="blogpost">
                <div class="card">
                    <div class="card-description">
                        <h3>The Introduction</h3>
                        <?php the_content(); ?>
                    </div>
                </div>
            </section>

            <aside id="sidebar">
                <h3>Sidebar Heading</h3>
                <p>Sidebar 1</p>
            </aside>
        </div>
    </main>
    <?php
    endwhile;
    else :
    endif; 
    get_footer();
?>