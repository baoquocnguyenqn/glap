<?php get_header();?>
    
    <main>
        <a href="blogslist.html">
            <h2 class="section-heading">All Blogs</h2>
        </a>

        <section>
        <?php 
        if ( have_posts() ) :
        while ( have_posts() ) : the_post();?>
        <div class="card">
                <div class="card-image">
                    <a href="<?php the_permalink(); ?>">
                        <img src="<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>" alt="Card Image">
                    </a>
                </div>

                <div class="card-description">
                    <a href="<?php the_permalink(); ?>">
                        <h3><?php the_title();  ?></h3>
                    </a>
                    <p>
                        <?php the_content();  ?>
                    </p>
                    <a href="<?php the_permalink(); ?>" class="btn-readmore">Read more</a>
                </div>
            </div>
        <?php endwhile;
       else :

        get_template_part( 'template-parts/content', 'none' );

    endif; ?>
           

           
        </section>

    
    
    </main>
<?php get_footer();?>

