 <!-- start image footer -->
 <section>
            <img class="card-img-top" src="<?php bloginfo('template_url'); ?>/assets/images/icon/footer.jpg" alt="">
        </section>
        <!-- end of image footer -->
        <!-- start footer -->
        <footer class="site-footer">
            <div class="back-to-top">
                <a href="#" class="back-to-top-btn"><i class="fa fa-arrow-up" aria-hidden="true"></i></a>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-sm-6">
                        <a href="/"><img class="img-logo" src="<?php bloginfo('template_url'); ?>/assets/images/icon/logo.png" alt=""></a>
                        <h3>Công ty Thương mại và Công nghệ BITI</h3>
                        <p>Chuyên thiết kế website, marketing online, thiết kế các ứng dụng chuyên nghiệp, app mobile.</p>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <h3>Dịch vụ</h3>
                        <p>Thiết kế Web đẹp chuẩn SEO Kho giao diện website đẹp SEO & Marketing website Chạy quảng cáo Google Ads Thiết kế ứng dụng Mobile.</p>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <h3>Liên hệ</h3>
                        <h4>VPĐD tại Việt Nam</h4>
                        <p>ĐC: 26 Tùng Lâm, Hòa Xuân, Cẩm Lệ, Đà Nẵng</p>
                        <p>Hotline: (+84) 794.98.8686</p>
                        <p>Email: nguyenbaonguyen.vn@gmail.comg</p>
                        <h4>VPĐD tại Pháp</h4>
                        <p>ĐC: 9 Allee jean baptiste clement, Andresy, France</p>
                        <p>Hotline: (+84) 794.98.8686</p>
                        <p>Email: nguyenbaonguyen.vn@gmail.com</p>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <h3>Theo dõi chúng tôi</h3>
                        <ul class="social-links">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                            <li><a href="#"><i class="fa fa-vimeo"></i></a></li>
                        </ul>
                    </div>
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </footer>
        <!-- end footer -->

    </div>
    <!-- end of page-wrapper -->

    <!-- All JavaScript files
    ================================================== -->
    <script src="<?php bloginfo('template_url'); ?>/assets/js/jquery.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/assets/js/bootstrap.min.js"></script>

    <!-- Custom script for this template -->
    <script src="<?php bloginfo('template_url'); ?>/assets/js/script.js?v=15"></script>
    <?php wp_footer(); ?>
</body>

<!-- Mirrored from irsfoundation.com/tf/templates/wedding/lovely-wedding/lovely-wedding/blog.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 09 Nov 2020 01:55:08 GMT -->

</html>