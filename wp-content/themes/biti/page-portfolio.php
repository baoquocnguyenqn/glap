<?php
/**
 * Template Name: Biti
 * Template Post Type: post, page
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */
?>
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from irsfoundation.com/tf/templates/wedding/lovely-wedding/lovely-wedding/blog.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 09 Nov 2020 01:54:38 GMT -->

<head>
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Page Title -->
    <title> Biti - Portfolio </title>

    <!-- Favicon and Touch Icons -->
    <link href="<?php bloginfo('template_url'); ?>/assets/images/icon/favicon.jpg" rel="shortcut icon" type="image/png">

    <!-- Icon fonts -->
    <link href="<?php bloginfo('template_url'); ?>/assets/css/font-awesome.min.css?v=7" rel="stylesheet">

    <!-- Bootstrap core CSS -->
    <link href="<?php bloginfo('template_url'); ?>/assets/css/bootstrap.min.css?v=6" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php bloginfo('template_url'); ?>/assets/css/style.css?v=11" rel="stylesheet">
    <link href="<?php bloginfo('template_url'); ?>/assets/css/portfilio/main.css?v=8" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- start page-wrapper -->
    <div class="page-wrapper">

        <!-- start preloader -->
        <div class="preloader">
            <div class="inner">
                <span class="icon"><i class="fa fa-spinner" aria-hidden="true"></i></span>
            </div>
        </div>
        <!-- end preloader -->
        <!-- Start header -->
        <div id="sticky-fix"></div>
        <header id="header" class="site-header header-style-1">
            <nav class="navigation navbar navbar-default">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="open-btn">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div class="couple-logo">
                            <a href="/"><img class="img-logo" src="<?php bloginfo('template_url'); ?>/assets/images/icon/logo.png" alt=""></a>
                        </div>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse navbar-right navigation-holder">
                        <button class="close-navbar"><i class="fa fa-close"></i></button>
                        <ul class="nav navbar-nav">
                            <li><a href="/">Trang chủ</a></li>
                            <li><a href="gioi-thieu.html">Giới thiệu</a></li>
                            <li><a href="dich-vu.html">Dịch vụ</a></li>
                            <li><a href="kho-giao-dien.html">Kho giao diện</a></li>
                            <li><a href="du-an.html">Dự án</a></li>
                            <li><a href="tin-tuc.html">Tin tức</a></li>
                            <li><a href="lien-he.html">Liên hệ</a></li>
                            <li><a href="tel:0366185588"><i class="fa fa-phone icon-phone" aria-hidden="true"></i> 0366.185.588</a></li>
                        </ul>
                    </div>
                    <!-- end of nav-collapse -->
                </div>
                <!-- end of container -->
            </nav>
        </header>
        <!-- end of header -->

        <div class="page-title">
            <div class="container">
                <div class="row">
                    <div class="col col-xs-12">
                        <h2>Dự án thiết kế website</h2>
                        <div class="breadcrumb">
                            <ul>
                                <li><a href="/">Trang chủ</a></li>
                                <li>Dự án</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end container -->
        </div>
        <!-- end page-title -->

        <!-- start portfolio-main -->
        <section class="portfolio-main section-padding">
            <div class="container">
                <div class="row">
                    <div class="col col-xs-12">
                        <div class="section-title">
                            <div class="vertical-line">
                                <span></span>
                                <h4>DỰ ÁN</h4>
                            </div>
                            <h2>CÁC DỰ ÁN ĐÃ THỰC HIỆN</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                        <li class="nav-item active">
                            <a class="nav-link " id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Giáo dục</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Bán hàng</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Bất động sản</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade active in" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                            <div class="col-lg-4 col-sm-6 ">
                                <div class="portfolio-item">
                                    <div class="card">
                                        <div class="image">
                                            <a href="#">
                                                <img class="card-img-top" src="<?php bloginfo('template_url'); ?>/assets/images/portfolio/project1.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <h4 class="card-title">
                                                <a href="#">Giáo dục 1</a>
                                            </h4>
                                            <a class="btn view-detail" target="_blank"  href="#" role="button">Xem chi tiết</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4 col-sm-6 ">
                                <div class="portfolio-item">
                                    <div class="card">
                                        <div class="image">
                                            <a href="#">
                                                <img class="card-img-top" src="<?php bloginfo('template_url'); ?>/assets/images/portfolio/project1.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <h4 class="card-title">
                                                <a href="#">Giáo dục 2</a>
                                            </h4>
                                            <a class="btn view-detail" target="_blank"  href="#" role="button">Xem chi tiết</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4 col-sm-6 ">
                                <div class="portfolio-item">
                                    <div class="card">
                                        <div class="image">
                                            <a href="#">
                                                <img class="card-img-top" src="<?php bloginfo('template_url'); ?>/assets/images/portfolio/project1.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <h4 class="card-title">
                                                <a href="#">Giáo dục 3</a>
                                            </h4>
                                            <a class="btn view-detail" target="_blank"  href="#" role="button">Xem chi tiết</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="view-more">
                                <button type="button" class="btn">Xem thêm</button>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                            <div class="col-lg-4 col-sm-6 ">
                                <div class="portfolio-item">
                                    <div class="card">
                                        <div class="image">
                                            <a href="#">
                                                <img class="card-img-top" src="<?php bloginfo('template_url'); ?>/assets/images/portfolio/project1.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <h4 class="card-title">
                                                <a href="#">Bán hàng 1</a>
                                            </h4>
                                            <a class="btn view-detail" target="_blank"  href="#" role="button">Xem chi tiết</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4 col-sm-6 ">
                                <div class="portfolio-item">
                                    <div class="card">
                                        <div class="image">
                                            <a href="#">
                                                <img class="card-img-top" src="<?php bloginfo('template_url'); ?>/assets/images/portfolio/project1.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <h4 class="card-title">
                                                <a href="#">Bán hàng 2</a>
                                            </h4>
                                            <a class="btn view-detail" target="_blank"  href="#" role="button">Xem chi tiết</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4 col-sm-6 ">
                                <div class="portfolio-item">
                                    <div class="card">
                                        <div class="image">
                                            <a href="#">
                                                <img class="card-img-top" src="<?php bloginfo('template_url'); ?>/assets/images/portfolio/project1.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <h4 class="card-title">
                                                <a href="#">Bán hàng 3</a>
                                            </h4>
                                            <a class="btn view-detail" target="_blank"  href="#" role="button">Xem chi tiết</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="view-more">
                                <button type="button" class="btn">Xem thêm</button>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                            <div class="col-lg-4 col-sm-6 ">
                                <div class="portfolio-item">
                                    <div class="card">
                                        <div class="image">
                                            <a href="#">
                                                <img class="card-img-top" src="<?php bloginfo('template_url'); ?>/assets/images/portfolio/project1.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <h4 class="card-title">
                                                <a href="#">Bất động sản 1</a>
                                            </h4>
                                            <a class="btn view-detail" target="_blank"  href="#" role="button">Xem chi tiết</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4 col-sm-6 ">
                                <div class="portfolio-item">
                                    <div class="card">
                                        <div class="image">
                                            <a href="#">
                                                <img class="card-img-top" src="<?php bloginfo('template_url'); ?>/assets/images/portfolio/project1.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <h4 class="card-title">
                                                <a href="#">Bất động sản 2</a>
                                            </h4>
                                            <a class="btn view-detail" target="_blank"  href="#" role="button">Xem chi tiết</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4 col-sm-6 ">
                                <div class="portfolio-item">
                                    <div class="card">
                                        <div class="image">
                                            <a href="#">
                                                <img class="card-img-top" src="<?php bloginfo('template_url'); ?>/assets/images/portfolio/project1.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <h4 class="card-title">
                                                <a href="#">Bất động sản 3</a>
                                            </h4>
                                            <a class="btn view-detail" target="_blank"  href="#" role="button">Xem chi tiết</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="view-more">
                                <button type="button" class="btn">Xem thêm</button>
                            </div>
                        </div>
                    </div>
                    <!-- end of post -->
                    <!-- end of blog-content -->
                </div>
                <!-- end of row -->
            </div>

            <!-- end of container -->
        </section>



        <!-- end of portfolio-main -->
        <!-- start image footer -->
        <section>
            <img class="card-img-top" src="<?php bloginfo('template_url'); ?>/assets/images/icon/footer.jpg" alt="">
        </section>
        <!-- end of image footer -->
        <!-- start footer -->
        <footer class="site-footer">
            <div class="back-to-top">
                <a href="#" class="back-to-top-btn"><i class="fa fa-arrow-up" aria-hidden="true"></i></a>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-sm-6">
                        <a href="/"><img class="img-logo" src="<?php bloginfo('template_url'); ?>/assets/images/icon/logo.png" alt=""></a>
                        <h3>Công ty Thương mại và Công nghệ BITI</h3>
                        <p>Chuyên thiết kế website, marketing online, thiết kế các ứng dụng chuyên nghiệp, app mobile.</p>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <h3>Dịch vụ</h3>
                        <p>Thiết kế Web đẹp chuẩn SEO Kho giao diện website đẹp SEO & Marketing website Chạy quảng cáo Google Ads Thiết kế ứng dụng Mobile.</p>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <h3>Liên hệ</h3>
                        <h4>VPĐD tại Việt Nam</h4>
                        <p>ĐC: 26 Tùng Lâm, Hòa Xuân, Cẩm Lệ, Đà Nẵng</p>
                        <p>Hotline: (+84) 794.98.8686</p>
                        <p>Email: nguyenbaonguyen.vn@gmail.comg</p>
                        <h4>VPĐD tại Pháp</h4>
                        <p>ĐC: 9 Allee jean baptiste clement, Andresy, France</p>
                        <p>Hotline: (+84) 794.98.8686</p>
                        <p>Email: nguyenbaonguyen.vn@gmail.com</p>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <h3>Theo dõi chúng tôi</h3>
                        <ul class="social-links">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                            <li><a href="#"><i class="fa fa-vimeo"></i></a></li>
                        </ul>
                    </div>
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </footer>
        <!-- end footer -->

    </div>
    <!-- end of page-wrapper -->

    <!-- All JavaScript files
    ================================================== -->
    <script src="<?php bloginfo('template_url'); ?>/assets/js/jquery.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/assets/js/bootstrap.min.js"></script>

    <!-- Custom script for this template -->
    <script src="<?php bloginfo('template_url'); ?>/assets/js/script.js?v=14"></script>
</body>

<!-- Mirrored from irsfoundation.com/tf/templates/wedding/lovely-wedding/lovely-wedding/blog.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 09 Nov 2020 01:55:08 GMT -->

</html>