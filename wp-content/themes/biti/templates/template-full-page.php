<?php
/**
 * Template Name: Tempalte full page biti
 * Template Post Type: post, page
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */
get_header();
?>


        <div class="page-title">
            <div class="container">
                <div class="row">
                    <div class="col col-xs-12">
                        <h2>Dự án thiết kế website</h2>
                        <div class="breadcrumb">
                            <ul>
                                <li><a href="/">Trang chủ</a></li>
                                <li>Dự án</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end container -->
        </div>
        <!-- end page-title -->

        <!-- start portfolio-main -->
        <section class="portfolio-main section-padding">
            <div class="container">
                <div class="row">
                    <div class="col col-xs-12">
                        <div class="section-title">
                            <div class="vertical-line">
                                <span></span>
                                <h4><?php the_title(); ?></h4>
                            </div>
                            <h2>CÁC DỰ ÁN ĐÃ THỰC HIỆN</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                <?php
                    if ( have_posts() ) :
                    while ( have_posts() ) : the_post();?>
                    <article class="post page">
                        <?php the_content(); ?>
                    </article>
                    <?php
                    endwhile;
                    else :
                    endif; 
                    ?>
                    <!-- end of post -->
                    <!-- end of blog-content -->
                </div>
                <!-- end of row -->
            </div>

            <!-- end of container -->
        </section>



        <!-- end of portfolio-main -->
        <?php get_footer(); ?>
