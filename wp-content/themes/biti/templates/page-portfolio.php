<?php
/**
 * Template Name: Biti
 * Template Post Type: post, page
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */
get_header();
?>


        <div class="page-title">
            <div class="container">
                <div class="row">
                    <div class="col col-xs-12">
                        <h2>Dự án thiết kế website</h2>
                        <div class="breadcrumb">
                            <ul>
                                <li><a href="/">Trang chủ</a></li>
                                <li>Dự án</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end container -->
        </div>
        <!-- end page-title -->

        <!-- start portfolio-main -->
        <section class="portfolio-main section-padding">
            <div class="container">
                <div class="row">
                    <div class="col col-xs-12">
                        <div class="section-title">
                            <div class="vertical-line">
                                <span></span>
                                <h4>DỰ ÁN</h4>
                            </div>
                            <h2>CÁC DỰ ÁN ĐÃ THỰC HIỆN</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                        <li class="nav-item active">
                            <a class="nav-link " id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Giáo dục</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Bán hàng</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Bất động sản</a>
                        </li>
                    </ul>
                 
						
                    <div class="block_search header-search-box">
                        <input name="txtSearch" type="text" id="txtSearch" class="search ui-autocomplete-input has-error header-search-input" autocomplete="off" onkeypress="return clickButtonHeader(event,'btnSearchText')" placeholder="Nhập từ khóa tìm kiếm">
                    
                        <a id="btnSearchText" href="javascript:void(0)" onclick="CheckValidateSearch()"><i class="fa fa-phone icon-phone" aria-hidden="true"></i></a>
                    </div>
						
					
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade active in" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                            <div class="col-lg-4 col-sm-6 ">
                                <div class="portfolio-item">
                                    <div class="card">
                                        <div class="image">
                                            <a href="#">
                                                <img class="card-img-top" src="<?php bloginfo('template_url'); ?>/assets/images/portfolio/project1.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <h4 class="card-title">
                                                <a href="#">Giáo dục 1</a>
                                            </h4>
                                            <a class="btn view-detail" target="_blank"  href="#" role="button">Xem chi tiết</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4 col-sm-6 ">
                                <div class="portfolio-item">
                                    <div class="card">
                                        <div class="image">
                                            <a href="#">
                                                <img class="card-img-top" src="<?php bloginfo('template_url'); ?>/assets/images/portfolio/project1.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <h4 class="card-title">
                                                <a href="#">Giáo dục 2</a>
                                            </h4>
                                            <a class="btn view-detail" target="_blank"  href="#" role="button">Xem chi tiết</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4 col-sm-6 ">
                                <div class="portfolio-item">
                                    <div class="card">
                                        <div class="image">
                                            <a href="#">
                                                <img class="card-img-top" src="<?php bloginfo('template_url'); ?>/assets/images/portfolio/project1.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <h4 class="card-title">
                                                <a href="#">Giáo dục 3</a>
                                            </h4>
                                            <a class="btn view-detail" target="_blank"  href="#" role="button">Xem chi tiết</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="view-more">
                                <button type="button" class="btn">Xem thêm</button>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                            <div class="col-lg-4 col-sm-6 ">
                                <div class="portfolio-item">
                                    <div class="card">
                                        <div class="image">
                                            <a href="#">
                                                <img class="card-img-top" src="<?php bloginfo('template_url'); ?>/assets/images/portfolio/project1.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <h4 class="card-title">
                                                <a href="#">Bán hàng 1</a>
                                            </h4>
                                            <a class="btn view-detail" target="_blank"  href="#" role="button">Xem chi tiết</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4 col-sm-6 ">
                                <div class="portfolio-item">
                                    <div class="card">
                                        <div class="image">
                                            <a href="#">
                                                <img class="card-img-top" src="<?php bloginfo('template_url'); ?>/assets/images/portfolio/project1.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <h4 class="card-title">
                                                <a href="#">Bán hàng 2</a>
                                            </h4>
                                            <a class="btn view-detail" target="_blank"  href="#" role="button">Xem chi tiết</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4 col-sm-6 ">
                                <div class="portfolio-item">
                                    <div class="card">
                                        <div class="image">
                                            <a href="#">
                                                <img class="card-img-top" src="<?php bloginfo('template_url'); ?>/assets/images/portfolio/project1.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <h4 class="card-title">
                                                <a href="#">Bán hàng 3</a>
                                            </h4>
                                            <a class="btn view-detail" target="_blank"  href="#" role="button">Xem chi tiết</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="view-more">
                                <button type="button" class="btn">Xem thêm</button>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                            <div class="col-lg-4 col-sm-6 ">
                                <div class="portfolio-item">
                                    <div class="card">
                                        <div class="image">
                                            <a href="#">
                                                <img class="card-img-top" src="<?php bloginfo('template_url'); ?>/assets/images/portfolio/project1.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <h4 class="card-title">
                                                <a href="#">Bất động sản 1</a>
                                            </h4>
                                            <a class="btn view-detail" target="_blank"  href="#" role="button">Xem chi tiết</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4 col-sm-6 ">
                                <div class="portfolio-item">
                                    <div class="card">
                                        <div class="image">
                                            <a href="#">
                                                <img class="card-img-top" src="<?php bloginfo('template_url'); ?>/assets/images/portfolio/project1.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <h4 class="card-title">
                                                <a href="#">Bất động sản 2</a>
                                            </h4>
                                            <a class="btn view-detail" target="_blank"  href="#" role="button">Xem chi tiết</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4 col-sm-6 ">
                                <div class="portfolio-item">
                                    <div class="card">
                                        <div class="image">
                                            <a href="#">
                                                <img class="card-img-top" src="<?php bloginfo('template_url'); ?>/assets/images/portfolio/project1.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <h4 class="card-title">
                                                <a href="#">Bất động sản 3</a>
                                            </h4>
                                            <a class="btn view-detail" target="_blank"  href="#" role="button">Xem chi tiết</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="view-more">
                                <button type="button" class="btn">Xem thêm</button>
                            </div>
                        </div>
                    </div>
                    <!-- end of post -->
                    <!-- end of blog-content -->
                </div>
                <!-- end of row -->
            </div>

            <!-- end of container -->
        </section>



        <!-- end of portfolio-main -->
        <?php get_footer(); ?>
