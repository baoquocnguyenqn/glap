<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<!-- Mirrored from irsfoundation.com/tf/templates/wedding/lovely-wedding/lovely-wedding/blog.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 09 Nov 2020 01:54:38 GMT -->

<head>
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Page Title -->
    <title> Biti - Portfolio </title>

    <!-- Favicon and Touch Icons -->
    <link href="<?php bloginfo('template_url'); ?>/assets/images/icon/favicon.jpg" rel="shortcut icon" type="image/png">

    <!-- Icon fonts -->
    <link href="<?php bloginfo('template_url'); ?>/assets/css/font-awesome.min.css?v=7" rel="stylesheet">

    <!-- Bootstrap core CSS -->
    <link href="<?php bloginfo('template_url'); ?>/assets/css/bootstrap.min.css?v=6" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php bloginfo('template_url'); ?>/assets/css/style.css?v=16" rel="stylesheet">
    <link href="<?php bloginfo('template_url'); ?>/assets/css/portfilio/main.css?v=9" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<?php wp_head(); ?>
</head>

<body>

    <!-- start page-wrapper -->
    <div class="page-wrapper">

        <!-- start preloader -->
        <div class="preloader">
            <div class="inner">
                <span class="icon"><i class="fa fa-spinner" aria-hidden="true"></i></span>
            </div>
        </div>
        <!-- end preloader -->
        <!-- Start header -->
		<div id="sticky-fix"></div>
        <header id="header" class="site-header header-style-1">
            <nav class="navigation navbar navbar-default">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="open-btn">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div class="couple-logo">
                            <a href="/"><img class="img-logo" src="<?php bloginfo('template_url'); ?>/assets/images/icon/logo.png" alt=""></a>
                        </div>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse navbar-right navigation-holder">
                        <button class="close-navbar"><i class="fa fa-close"></i></button>
                        <ul class="nav navbar-nav">
                         <?php 
							$args = array(
                                'theme_location'=>'primary',
							);
						?>
						<?php wp_nav_menu($args); ?>   
                            <li><a href="tel:0366185588"><i class="fa fa-phone icon-phone" aria-hidden="true"></i> 0366.185.588</a></li>
                        </ul>
                    </div>
                    <!-- end of nav-collapse -->
                </div>
                <!-- end of container -->
            </nav>
        </header>
		 <!-- end of header -->