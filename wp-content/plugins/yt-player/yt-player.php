<?php

/*
 * Plugin Name: YT Player
 * Plugin URI:  http://bplugins.com
 * Description: A simple, accessable, fully customizable & user friendly YouTube Video Player for wordrpess.
 * Version: 1.3
 * Author: bPlugins LLC
 * Author URI: http://bPlugins.com
 * License: GPLv3
 * Text Domain: ytp
 * Domain Path:  /languages
 */
//--------------Fremius Integration------------------

if ( !function_exists( 'ytp_fs' ) ) {
    // Create a helper function for easy SDK access.
    function ytp_fs()
    {
        global  $ytp_fs ;
        
        if ( !isset( $ytp_fs ) ) {
            // Activate multisite network integration.
            if ( !defined( 'WP_FS__PRODUCT_5836_MULTISITE' ) ) {
                define( 'WP_FS__PRODUCT_5836_MULTISITE', true );
            }
            // Include Freemius SDK.
            require_once dirname( __FILE__ ) . '/freemius/start.php';
            $ytp_fs = fs_dynamic_init( array(
                'id'             => '5836',
                'slug'           => 'yt-player',
                'type'           => 'plugin',
                'public_key'     => 'pk_829fc74e7bb67d3d555c68048933d',
                'is_premium'     => false,
                'has_addons'     => false,
                'has_paid_plans' => true,
                'trial'          => array(
                'days'               => 7,
                'is_require_payment' => true,
            ),
                'menu'           => array(
                'slug' => 'edit.php?post_type=ytplayer',
            ),
                'is_live'        => true,
            ) );
        }
        
        return $ytp_fs;
    }
    
    // Init Freemius.
    ytp_fs();
    // Signal that SDK was initiated.
    do_action( 'ytp_fs_loaded' );
}

/*Some Set-up*/
define( 'YTP_PLUGIN_DIR', WP_PLUGIN_URL . '/' . plugin_basename( dirname( __FILE__ ) ) . '/' );
define( 'YTP_PLUGIN_VERSION', '1.2' );
// load text domain
function ytp_load_textdomain()
{
    load_plugin_textdomain( 'ytp', false, YTP_PLUGIN_DIR . "languages" );
}

add_action( "plugins_loaded", 'ytp_load_textdomain' );
/* JS & CSS*/

if ( !function_exists( 'ytp_get_script' ) ) {
    function ytp_get_script()
    {
        wp_enqueue_style(
            'ytp-style',
            plugin_dir_url( __FILE__ ) . 'public/css/player-style.css',
            array(),
            YTP_PLUGIN_VERSION,
            'all'
        );
        wp_enqueue_script(
            'ytp-js',
            plugin_dir_url( __FILE__ ) . 'public/js/yt-plyr.js',
            YTP_PLUGIN_VERSION,
            false
        );
    }
    
    add_action( 'wp_enqueue_scripts', 'ytp_get_script' );
}

/*-------------------------------------------------------------------------------*/
/* HIDE everything in PUBLISH metabox except Move to Trash & PUBLISH button
/*-------------------------------------------------------------------------------*/
function ytp_hide_publishing_actions()
{
    $my_post_type = 'ytplayer';
    global  $post ;
    if ( $post->post_type == $my_post_type ) {
        echo  '
                <style type="text/css">
                    #misc-publishing-actions,
                    #minor-publishing-actions{
                        display:none;
                    }
                </style>
            ' ;
    }
}

add_action( 'admin_head-post.php', 'ytp_hide_publishing_actions' );
add_action( 'admin_head-post-new.php', 'ytp_hide_publishing_actions' );
//   Hide & Disabled View, Quick Edit and Preview Button
function ytp_remove_row_actions( $idtions )
{
    global  $post ;
    
    if ( $post->post_type == 'ytplayer' ) {
        unset( $idtions['view'] );
        unset( $idtions['inline hide-if-no-js'] );
    }
    
    return $idtions;
}

if ( is_admin() ) {
    add_filter(
        'post_row_actions',
        'ytp_remove_row_actions',
        10,
        2
    );
}
//Remove post update massage and link
function ytp_updated_messages( $messages )
{
    $messages['ytplayer'][1] = __( 'Updated' );
    return $messages;
}

add_filter( 'post_updated_messages', 'ytp_updated_messages' );
/* Register Custom Post Types */
add_action( 'init', 'ytp_create_post_type' );
function ytp_create_post_type()
{
    register_post_type( 'ytplayer', array(
        'labels'              => array(
        'name'          => __( 'YT Players' ),
        'singular_name' => __( 'YT Player' ),
        'add_new'       => __( 'Add New Player' ),
        'add_new_item'  => __( 'Add new' ),
        'edit_item'     => __( 'Edit' ),
        'new_item'      => __( 'New' ),
        'view_item'     => __( 'View' ),
        'search_items'  => __( 'Search' ),
        'not_found'     => __( 'Sorry, we couldn\'t find any item you are looking for.' ),
    ),
        'public'              => false,
        'show_ui'             => true,
        'publicly_queryable'  => true,
        'exclude_from_search' => true,
        'menu_position'       => 14,
        'menu_icon'           => YTP_PLUGIN_DIR . 'img/icon.png',
        'has_archive'         => false,
        'hierarchical'        => false,
        'capability_type'     => 'page',
        'rewrite'             => array(
        'slug' => 'ytplayer',
    ),
        'supports'            => array( 'title' ),
    ) );
}

// ONLY OUR CUSTOM TYPE POSTS
add_filter( 'manage_ytplayer_posts_columns', 'ytp_column_handler', 10 );
add_action(
    'manage_ytplayer_posts_custom_column',
    'ytp_column_content_handler',
    10,
    2
);
// CREATE TWO FUNCTIONS TO HANDLE THE COLUMN
function ytp_column_handler( $defaults )
{
    $defaults['directors_name'] = 'ShortCode';
    return $defaults;
}

function ytp_column_content_handler( $column_name, $post_ID )
{
    if ( $column_name == 'directors_name' ) {
        // show content of 'directors_name' column
        echo  '<input onClick="this.select();" value="[ytplayer id=' . $post_ID . ']" >' ;
    }
}

//ShortCode Page
add_action( 'admin_menu', 'ytp_shortcode_page' );
function ytp_shortcode_page()
{
    add_submenu_page(
        'edit.php?post_type=ytplayer',
        'ShortCode',
        'ShortCode',
        'manage_options',
        'shortcode',
        'ytp_shortcode_page_callback'
    );
}

function ytp_shortcode_page_callback()
{
    echo  '<div class="wrap"><div id="icon-tools" class="icon32"></div>' ;
    echo  '<h2>ShortCode for Post / Pages or Widgets</h2>' ;
    echo  'Now you can embed any YouTube video using the shortcode below !' ;
    echo  '<br/><br/><input type="text" style="font-size: 12px; border: none; box-shadow: none; padding: 4px 8px; width:100%; background:#1e8cbe; color:white;"  onfocus="this.select();" readonly="readonly"  value="[ytp] YouTube Video URL [/ytp]" /><br/><br/>Video URL must be formatted like that: https://www.youtube.com/watch?v=NGvVtSXcZK4' ;
    echo  '</div>' ;
}

//How to use Page
add_action( 'admin_menu', 'ytp_howto_page' );
function ytp_howto_page()
{
    add_submenu_page(
        'edit.php?post_type=ytplayer',
        'How To Use',
        'How To Use',
        'manage_options',
        'ytp_howto',
        'ytp_howto_page_callback'
    );
}

function ytp_howto_page_callback()
{
    echo  '<div class="wrap"><div id="icon-tools" class="icon32"></div>' ;
    echo  '<h2>How to use ? </h2>
		<h2>Watch the video to learn how to use the plugin. </h2>
		<br/>
		<iframe width="789" height="375" src="https://www.youtube.com/embed/NGvVtSXcZK4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
		<br />
		
		' ;
    echo  '</div>' ;
}

//Developer Page
add_action( 'admin_menu', 'ytp_dev_page' );
function ytp_dev_page()
{
    add_submenu_page(
        'edit.php?post_type=ytplayer',
        'Developer',
        'Developer',
        'manage_options',
        'developer',
        'ytp_dev_page_callback'
    );
}

function ytp_dev_page_callback()
{
    echo  '<div class="wrap"><div id="icon-tools" class="icon32"></div>' ;
    echo  '<h2>Developer</h2>
		<h2>Md Abu hayat polash</h2>
		<h3>Professional Web Developer</h3>
		<h5>Hire Me : <a href="http://fiverr.com/abuhayat">www.fiverr.com/abuhayat</h5></a>
		Email: <a href="mailto:abuhayat.du@gmail.com">abuhayat.du@gmail.com </a>
		<h5>Skype: ah_polash</h5>
		<h5>Web : <a target="_blank" href="http://abuhayatpolash.com">www.abuhayatpolash.com</a></h5>
		<br />
		
		' ;
    echo  '</div>' ;
}


if ( !ytp_fs()->is_premium() ) {
    function ytp_add_review_metabox()
    {
        add_meta_box(
            'myplugin',
            __( 'Get 50% Off !', 'ytp' ),
            'ytp_review',
            'ytplayer',
            'side',
            'low'
        );
    }
    
    add_action( 'add_meta_boxes', 'ytp_add_review_metabox' );
    function ytp_review()
    {
        echo  '
<ul style="list-style-type: square;padding-left:10px;">
	<li><a href="https://wordpress.org/support/plugin/yt-player/reviews/?filter=5#new-post" target="_blank">&#9733;&#9733;&#9733;&#9733;&#9733; Rate </a> <strong>YT Player</strong> Plugin</li>
	<li>Take a screenshot along with your name and the comment. </li>
	<li><a href="mailto:ytplayerfeedback@gmail.com">Email us</a> ( ytplayerfeedback@gmail.com ) the screenshot.</li>
	<li>You will receive a promo code of 50% Off !</li>
</ul>	
 Your Review is very important to us as it helps us to grow more.</p>

<p>Not happy, Sorry for that &#128546; You can request for improvement. </p>

<table>
	<tr>
		<td><a class="button button-primary button-large" href="https://wordpress.org/support/plugin/yt-player/reviews/?filter=5#new-post" target="_blank">Write Review</a></td>
		<td><a class="button button-primary button-large" href="mailto:abuhayat.du@gmail.com" target="_blank">Request Improvement</a></td>
	</tr>
</table>
' ;
    }

}

// Footer Review Request
add_filter( 'admin_footer_text', 'ytp_admin_footer' );
function ytp_admin_footer( $text )
{
    
    if ( 'ytplayer' == get_post_type() ) {
        $url = 'https://wordpress.org/support/plugin/yt-player/reviews/?filter=5#new-post';
        $text = sprintf( __( 'If you like <strong>YT Player</strong> please leave us a <a href="%s" target="_blank">&#9733;&#9733;&#9733;&#9733;&#9733;</a> rating. Your Review is very important to us as it helps us to grow more. ', 'post-carousel' ), $url );
    }
    
    return $text;
}

// Add shortcode area
add_action( 'edit_form_after_title', 'ytp_shortcode_area' );
function ytp_shortcode_area()
{
    global  $post ;
    
    if ( $post->post_type == 'ytplayer' ) {
        ?>	
<div>
	<label style="cursor: pointer;font-size: 13px; font-style: italic;" for="ytp_shortcode">Copy this shortcode and paste it into your post, page, or text widget content:</label>
	<span style="display: block; margin: 5px 0; background:#1e8cbe; ">
		<input type="text" id="ytp_shortcode" style="font-size: 12px; border: none; box-shadow: none;padding: 4px 8px; width:100%; background:transparent; color:white;"  onfocus="this.select();" readonly="readonly"  value="[ytplayer id=<?php 
        echo  $post->ID ;
        ?>]" /> 
		
	</span>
</div>
 <?php 
    }

}

/*-------------------------------------------------------------------------------*/
/* Lets register our shortcode
/*-------------------------------------------------------------------------------*/
add_shortcode( 'ytplayer', 'ytp_shortcode_func' );
function ytp_shortcode_func( $atts )
{
    extract( shortcode_atts( array(
        'id' => null,
    ), $atts ) );
    ?>

<?php 
    Ob_start();
    ?>
<div style="<?php 
    $width = get_post_meta( $id, '_ytp_video_width', true );
    
    if ( $width == '0' ) {
        $width = 'width:100%;';
    } else {
        $width = 'max-width:' . $width . 'px;';
    }
    
    echo  $width ;
    ?>">
<style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%;} .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style>

<div class="plyr__video-embed embed-container" id="player<?php 
    echo  $id ;
    ?>">
    <iframe
        src="https://www.youtube.com/embed/<?php 
    echo  get_post_meta( $id, '_ytp_video_id', true ) ;
    ?>"
        
		allowfullscreen
        allowtransparency
        allow="autoplay"
    ></iframe>
</div>

<script type="text/javascript">

const player<?php 
    echo  $id ;
    ?> = new Plyr('#player<?php 
    echo  $id ;
    ?>', {
	clickToPlay:false,
	disableContextMenu: false,
	controls:[
	<?php 
    $stutas = get_post_meta( $id, '_ytp_large_play', true );
    if ( $stutas !== "on" ) {
        echo  "'play-large'," ;
    }
    ?>
	<?php 
    $stutas = get_post_meta( $id, '_ytp_play', true );
    if ( $stutas !== "on" ) {
        echo  "'play'," ;
    }
    ?>
	<?php 
    $stutas = get_post_meta( $id, '_ytp_progress_bar', true );
    if ( $stutas !== "on" ) {
        echo  "'progress'," ;
    }
    ?>
	<?php 
    $stutas = get_post_meta( $id, '_ytp_duration', true );
    if ( $stutas !== "on" ) {
        echo  "'duration'," ;
    }
    ?>
	<?php 
    $stutas = get_post_meta( $id, '_ytp_mute_button', true );
    if ( $stutas !== "on" ) {
        echo  " 'mute'," ;
    }
    ?>
	<?php 
    $stutas = get_post_meta( $id, '_ytp_volume_control', true );
    if ( $stutas !== "on" ) {
        echo  "'volume'," ;
    }
    ?>	
	<?php 
    $stutas = get_post_meta( $id, '_ytp_setting', true );
    if ( $stutas !== "on" ) {
        echo  "'settings'," ;
    }
    ?>	
	<?php 
    $stutas = get_post_meta( $id, '_ytp_video_fs', true );
    if ( $stutas !== "on" ) {
        echo  "'fullscreen'," ;
    }
    ?>	
],

	youtube: { noCookie: true, rel: 0, showinfo: 0, iv_load_policy: 3, modestbranding: 0} 
});

</script>

</div>

<?php 
    $output = ob_get_clean();
    return $output;
    ?>

<?php 
}

// Our second shortCode
add_shortcode( 'ytp', 'ytp_shortcode_function' );
function ytp_shortcode_function( $atts, $content = null )
{
    extract( shortcode_atts( array(
        'url' => null,
    ), $atts ) );
    ?>

<?php 
    Ob_start();
    $id = str_replace( 'https://www.youtube.com/watch?v=', '', $content );
    $selector = uniqid();
    ?>
<div style="<?php 
    $width = get_post_meta( $id, '_ytp_video_width', true );
    
    if ( $width == '0' ) {
        $width = 'width:100%;';
    } else {
        $width = 'max-width:' . $width . 'px;';
    }
    
    echo  $width ;
    ?>">
<style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%;} .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style>

<div class="plyr__video-embed embed-container" id="p<?php 
    echo  $selector ;
    ?>">
    <iframe
        src="<?php 
    echo  'https://www.youtube.com/embed/' . $id ;
    ?>"
        allowfullscreen
        allowtransparency
        allow="autoplay"
    ></iframe>
</div>

<script type="text/javascript">

const p<?php 
    echo  $selector ;
    ?> = new Plyr('#p<?php 
    echo  $selector ;
    ?>', {
	clickToPlay:false,
	disableContextMenu: false,
	youtube:{ noCookie: true, rel: 0, showinfo: 0, iv_load_policy: 3, modestbranding: 1 }
});

</script>

</div>

<?php 
    $output = ob_get_clean();
    return $output;
    ?>

<?php 
}

//  INC
include_once 'admin/metabox/meta-box-class/my-meta-box-class.php';
include_once 'admin/metabox/class-usage-demo.php';