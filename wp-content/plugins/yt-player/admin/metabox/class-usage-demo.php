<?php
//include the main class file
require_once("meta-box-class/my-meta-box-class.php");
if (is_admin()){
  /* 
   * prefix of meta keys, optional
   * use underscore (_) at the beginning to make keys hidden, for example $prefix = '_ba_';
   *  you also can make prefix empty to disable it
   * 
   */
  $prefix = '_ytp_';


  $config2 = array(
    'id'             => 'demo_meta_box2',          // meta box id, unique per meta box
    'title'          => 'YouTube Player Setup',          // meta box title
    'pages'          => array('ytplayer'),      // post types, accept custom post types as well, default is array('post'); optional
    'context'        => 'normal',            // where the meta box appear: normal (default), advanced, side; optional
    'priority'       => 'high',            // order of meta box: high (default), low; optional
    'fields'         => array(),            // list of meta fields (can be added by field arrays)
    'local_images'   => false,          // Use local or hosted images (meta box images for add/remove)
    'use_with_theme' => false          //change path if used with theme set to true, false for a plugin or anything else for a custom path(default false).
  );
  
  
  /*
   * Initiate your 2nd meta box
   */
  $my_meta2 =  new ytp_Meta_Box($config2);

 $my_meta2->addText($prefix.'video_id',array('name'=> 'YouTube video id (Required)', 'desc' =>'Paste youtube video ID,  example: for https://www.youtube.com/watch?v=ScMzIvxBSi4 the id is ScMzIvxBSi4'));
  $my_meta2->addNumber($prefix.'video_width',array('name'=> 'Player Width', 'desc' =>'Set Player width. Enter 600 If you want a player with 600px width. Leave 0 if you want a RESPONSIVE player. ','std'=>'0'));
$my_meta2->addCheckbox($prefix.'large_play',array('name'=> 'Hide large play button','desc' =>'Check if you want to hide the Large play button from the player center.'));
$my_meta2->addCheckbox($prefix.'play',array('name'=> 'Hide Play button','desc' =>'Check if you want to hide the play button in the player'));		
$my_meta2->addCheckbox($prefix.'progress_bar',array('name'=> 'Hide Video Progress bar','desc' =>'Check if you want to hide the video progress bar in the player'));	
$my_meta2->addCheckbox($prefix.'duration',array('name'=> 'Hide Video Duration','desc' =>'Check if you want to hide the video duration in the player'));	
$my_meta2->addCheckbox($prefix.'mute_button',array('name'=> 'Hide Mute Button','desc' =>'Check if you want to hide the mute button in the player'));
$my_meta2->addCheckbox($prefix.'volume_control',array('name'=> 'Hide Volume control','desc' =>'Check if you want to hide the volume control in the player'));	
$my_meta2->addCheckbox($prefix.'setting',array('name'=> 'Hide Settings button','desc' =>'Check if you want to hide the setting button in the player'));	  	
$my_meta2->addCheckbox($prefix.'video_fs',array('name'=> 'Hide Full Screen Button','desc' =>'Prevents the fullscreen button from displaying in the player.'));	

  //Finish Meta Box Declaration 
  $my_meta2->Finish(); 
}