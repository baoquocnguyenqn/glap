<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'glaplatform_education' );

/** MySQL database username */
define( 'DB_USER', 'glaplatform_education' );

/** MySQL database password */
define( 'DB_PASSWORD', 'glap123!@#' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'K0uhJH{r)m3@h%ycus]Hma$!#{ qXeOsJ)2}XqIq<M3U*1mqx{rdjd,>( LtLP M' );
define( 'SECURE_AUTH_KEY',  'CJx+{F6w~65i!dX?(w{Q1=kH7m{0>.4*C0_:IB-t}%^p2QV.t0J53DK:O%cpJQ*7' );
define( 'LOGGED_IN_KEY',    'H*PHXcCDHuYQH|;.}c@eUvz9C}o UE._fHGM(jDFlf@YK2x!mZ[}0Q/:rl6MVs[-' );
define( 'NONCE_KEY',        'OR>jq#+0p?$Pxc078`ou/+xWx p[D17y<]|,9W%#PzuZkXU[Yx!rMqWuc$=I/n>~' );
define( 'AUTH_SALT',        '#K[bpcMo%#F,A&mp+lslSj}un2z2oOF3+kM8/+lj|,Q1a&T;g.p/yO[XD}dFzK_~' );
define( 'SECURE_AUTH_SALT', '%I+cs-YAw=r03W..,Vv.$lI]>J:7>o-@z4S.}YAC?E+3d*j_,xE<jL|5n#+77$^v' );
define( 'LOGGED_IN_SALT',   'IaZS_}/JqtO<X#!JV;nhQ!sJ<pRFgy)Ew5:@m9pzq}|J+/rt/B_RMXqZmcl@sJ5*' );
define( 'NONCE_SALT',       '2pjyEicd~%p8*v1j<=chBg!QGcV]+0at1XTj@:A]NM5<l<jm<66Q-K@dA]/e,+^:' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_biti';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
